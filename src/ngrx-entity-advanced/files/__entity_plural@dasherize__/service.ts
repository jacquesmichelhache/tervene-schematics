import { Observable } from 'rxjs'
import { IndexResponse } from '../index-response'
import { setHttpParams } from 'src/app/helpers/http-helpers'
import { Injectable } from '@angular/core'
import { Store } from '@ngrx/store'
import { BaseEntityService } from '../base-entity-service'
import { <%= capitalize(entity_singular)%> } from './classes'
import * as <%= capitalize(entity_plural)%>Actions from './actions'
import { EntityOperationsHelper } from '../entity-operations-helper'
import {
  ActionOptions,
  ClearEntities,
  CreateActionOptions,
  DeleteActionOptions,
  GetActionOptions,
  LoadActionOptions,
  UpdateActionOptions,
} from '../shared/advanced-store-classes'

// data service. This is where http requests are sent to API
@Injectable({ providedIn: 'root' })
export class <%= capitalize(entity_plural)%>DataService extends BaseEntityService {
  get<%= capitalize(entity_plural)%>$(params?: object, options?: LoadActionOptions): Observable<IndexResponse<<%= capitalize(entity_singular)%>>> {
    return this.http.get<IndexResponse<<%= capitalize(entity_singular)%>>>(`${this.tenantApiUrl}/<%= underscore(entity_plural) %>`, { params: setHttpParams({...params, ...options?.queryParams}) })
  }

  get<%= capitalize(entity_singular)%>$(id: number, options?: GetActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.get<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural) %>/${id}`, { params: setHttpParams(options?.queryParams) })
  }

  create<%= capitalize(entity_singular)%>$(data: Partial<<%= capitalize(entity_singular)%>> | FormData, options?: CreateActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.post<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural) %>`, { <%= underscore(entity_singular) %>: data }, { params: setHttpParams(options?.queryParams) })
  }

  update<%= capitalize(entity_singular)%>$(id: number, data: Partial<<%= capitalize(entity_singular)%>> | FormData, options?: UpdateActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.patch<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural) %>/${id}`, { <%= underscore(entity_singular) %>: data }, { params: setHttpParams(options?.queryParams) })
  }

  delete<%= capitalize(entity_singular)%>$(id: number, options?: DeleteActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.delete<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural) %>/${id}`, { params: setHttpParams(options?.queryParams) })
  }
}

// Entity operations service facad implementing correlationId
@Injectable({ providedIn: 'root' })
export class <%= capitalize(entity_plural)%>EntityOperationsService {
  constructor(private rxStore: Store, private entityOperationsHelper: EntityOperationsHelper) {}

  getAll(options?: LoadActionOptions): Observable<IndexResponse<<%= capitalize(entity_singular)%>>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<IndexResponse<<%= capitalize(entity_singular)%>>>(
      corrId,
      <%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS,
      <%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS_CACHE,
      <%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_FAIL,
    )
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>({ page: 0, page_size: -1 }, { ...options, corrId: corrId }))
    return result$
  }

  getWithQuery(params: Partial<<%= capitalize(entity_singular)%>> | object, options?: LoadActionOptions): Observable<IndexResponse<<%= capitalize(entity_singular)%>>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<IndexResponse<<%= capitalize(entity_singular)%>>>(
      corrId,
      <%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS,
      <%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS_CACHE,
      <%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_FAIL,
    )
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>(params, { ...options, corrId: corrId }))
    return result$
  }

  getById(id: number, options?: GetActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(
      corrId,
      <%= capitalize(entity_plural)%>Actions.GET_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS,
      <%= capitalize(entity_plural)%>Actions.GET_<%= underscore(entity_singular).toUpperCase() %>_FAIL,
    )
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>(id, { ...options, corrId: corrId }))
    return result$
  }

  add(<%= capitalize(entity_singular)%>: Partial<<%= capitalize(entity_singular)%>>, options?: CreateActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(
      corrId,
      <%= capitalize(entity_plural)%>Actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS,
      <%= capitalize(entity_plural)%>Actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL,
    )
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>(<%= capitalize(entity_singular)%>, { ...options, corrId: corrId }))
    return result$
  }

  update(id: number, <%= capitalize(entity_singular)%>: Partial<<%= capitalize(entity_singular)%>>, options?: UpdateActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(
      corrId,
      <%= capitalize(entity_plural)%>Actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS,
      <%= capitalize(entity_plural)%>Actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL,
    )
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>(id, <%= capitalize(entity_singular)%>, { ...options, corrId: corrId }))
    return result$
  }

  delete(id: number, options?: DeleteActionOptions): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(
      corrId,
      <%= capitalize(entity_plural)%>Actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS,
      <%= capitalize(entity_plural)%>Actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>_FAIL,
    )
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>(id, { ...options, corrId: corrId }))
    return result$
  }

  // helpers
  setMany(entities: <%= capitalize(entity_singular)%>[], options?: ActionOptions) {
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.SetMany<%= capitalize(entity_plural)%>(entities, options))
  }
  setOne(entity: <%= capitalize(entity_singular)%>, options?: ActionOptions) {
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.SetOne<%= capitalize(entity_singular)%>(entity, options))
  }
  select(id: number) {
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(id))
  }
  unselect() {
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Unselect<%= capitalize(entity_singular)%>())
  }
  clear(options?: ClearEntities) {
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Clear<%= capitalize(entity_plural)%>(options))
  }
}

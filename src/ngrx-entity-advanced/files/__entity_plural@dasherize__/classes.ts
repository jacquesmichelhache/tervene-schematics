import { Entity } from "../shared/advanced-store-classes";

export class <%= capitalize(entity_singular)%> implements Entity {
 id: number
}
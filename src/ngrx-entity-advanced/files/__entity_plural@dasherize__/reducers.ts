import { EntitiesStateAdvanced } from '../shared/advanced-store-classes'
import {
  changeManyReducer,
  changeOneReducer,
  removeOneReducer,
  removeManyReducer,
  updateQueryCache,
  setQueryCacheEntry,
  invalidateQueryCache,
} from '../shared/advanced-store-reducer-methods'
import { getEntities, getEntitiesByIds, getEntitiesByQuery } from '../shared/advanced-store-selector-methods'
import * as actions from './actions'
import { <%= capitalize(entity_singular)%> } from './classes'

export const initialState: EntitiesStateAdvanced<<%= capitalize(entity_singular)%>> = {
  entities: {}, // dictionary of entities accessible by id
  ids: [], // array of entity ids.
  selectedId: null,
  queryCache: {},
  entitiesMetadata: {}, // not implemented yet
}

export function reducer(state = initialState, action: actions.Actions): EntitiesStateAdvanced<Partial<<%= capitalize(entity_singular)%>>> {
  switch (action.type) {
    case actions.CLEAR_<%= underscore(entity_plural).toUpperCase() %>:
      return action.options?.onlyData ? { ...state, entities: {}, ids: [], queryCache: {} } : { ...initialState }
    case actions.UNSELECT_<%= underscore(entity_singular).toUpperCase() %>:
      return { ...state, selectedId: null }
    case actions.SELECT_<%= underscore(entity_singular).toUpperCase() %>:
      return state.selectedId != action.entityId ? { ...state, selectedId: action.entityId } : state    
    case actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>:
      return { ...state, queryCache: setQueryCacheEntry(state, action) }
    case actions.GET_<%= underscore(entity_singular).toUpperCase() %>:
    case actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>:
    case actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>:
    case actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>:
      return state
    case actions.SET_ONE_<%= underscore(entity_singular).toUpperCase() %>:
    case actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS:
    case actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS:
    case actions.GET_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS:
      return { ...changeOneReducer(state, action.payload, action.options) }
    case actions.SET_MANY_<%= underscore(entity_plural).toUpperCase() %>:
      return { ...changeManyReducer(state, action.payload, action.options) }
    case actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS:
      return {...changeManyReducer(state, action.payload.data, action.options), queryCache: updateQueryCache(state, action)}
    case actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS_CACHE:
      return state
    case actions.REMOVE_ONE_<%= underscore(entity_singular).toUpperCase() %>:
      return { ...removeOneReducer(state, action.id) }
    case actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS:
      return { ...removeOneReducer(state, action.payload.id) }
    case actions.REMOVE_MANY_<%= underscore(entity_plural).toUpperCase() %>:
      return { ...removeManyReducer(state, action.ids) }   
    case actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>_FAIL:
      return { ...state, queryCache: invalidateQueryCache(state, action) }
    case actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>_FAIL:
    case actions.GET_<%= underscore(entity_singular).toUpperCase() %>_FAIL:
    case actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL:
      return state
    case actions.ACTION_DONE:
      return { ...state, ...action.newState }
    default:
      return state
  }
}

export const get<%= capitalize(entity_plural)%>Dict = (state: EntitiesStateAdvanced<<%= capitalize(entity_singular)%>>) => state.entities
export const get<%= capitalize(entity_plural)%>ByIds = (entities: Record<number, <%= capitalize(entity_singular)%>>, ids: number[]) => getEntitiesByIds(entities, ids)
export const get<%= capitalize(entity_singular)%>ById = (entities: Record<number, <%= capitalize(entity_singular)%>>, id: number) => entities[+id]
export const getSelected<%= capitalize(entity_singular)%>Id = (state: EntitiesStateAdvanced<<%= capitalize(entity_singular)%>>) => state.selectedId
export const getSelected<%= capitalize(entity_singular)%> = (id: number, entities: Record<number, <%= capitalize(entity_singular)%>>) => entities[id]
export const getByQuery = (
  entities: Record<number, <%= capitalize(entity_singular)%>>,
  filterCallback: (entity: <%= capitalize(entity_singular)%>) => boolean,
  sortFn?: (a: <%= capitalize(entity_singular)%>, b: <%= capitalize(entity_singular)%>) => number,
) => getEntitiesByQuery<<%= capitalize(entity_singular)%>>(entities, filterCallback, sortFn)

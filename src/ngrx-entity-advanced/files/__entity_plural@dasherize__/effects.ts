import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { of, from, Observable } from 'rxjs'
import { catchError, withLatestFrom, concatMap, mergeMap, map } from 'rxjs/operators'
import * as EntitiesReducers from '../reducers'
import * as <%= capitalize(entity_plural)%>Actions from './actions'
import { TranslateService } from '@ngx-translate/core'
import { <%= capitalize(entity_plural)%>DataService } from './service'
import { BaseEffectsClass } from 'src/app/store/base-effects-class'
import { TerveneNotificationsService } from '../shared/services/notifications.service'
import { queryCacheAvailable } from '../shared/advanced-store-effect-methods'
import { EntityOperationsHelper } from '../entity-operations-helper'

@Injectable()
export class <%= capitalize(entity_plural)%>Effects extends BaseEffectsClass {

  constructor(
    router: Router,
    rxStore: Store<any>,
    private actions$: Actions,
    private <%= entity_plural%>DataService: <%= capitalize(entity_plural)%>DataService,
    private translate: TranslateService,
    private notification: TerveneNotificationsService,
    private entityOperationsHelper: EntityOperationsHelper
  ) {
    super(router, rxStore)
  }

  load<%= capitalize(entity_plural)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>>(<%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>),
      concatMap(action => of(action).pipe(withLatestFrom(this.rxStore.select(EntitiesReducers.get<%= capitalize(entity_plural)%>State)))),
      mergeMap(([action, state]) => {
        if (action.options?.queryCaching) {
          if(queryCacheAvailable(state, action)){
            return this.entityOperationsHelper.getCachedPayload$(state, action).pipe(
              map(cachedPayload => new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>SuccessCache(cachedPayload, action.options)),
              catchError(cacheAction => of(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>Fail({ ...cacheAction.options, errors: 'cache fail' })))
            )         
          }
        }    

        return this.<%= entity_plural%>DataService.get<%= capitalize(entity_plural)%>$(action.params, action.options).pipe(
          mergeMap(response => {
            return of(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>Success(response, { ...action.options, queryParams: action.params }))
          }),
          catchError(error => {
            this.handleErrorNotification(error, action.options)          
            return of(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>Fail({ ...action.options, errors: error }))
          }),
        )
      }),
    ),
  )

  get<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.GET_<%= underscore(entity_singular).toUpperCase() %>),
      concatMap(action => of(action).pipe(withLatestFrom(this.rxStore.select(EntitiesReducers.get<%= capitalize(entity_plural)%>State)))),
      mergeMap(([action, state]) => {
        const actions: Action[] = []

        // if cache is allowed, return the cache value instead of querying the API
        if (action.options?.useCache) {
          const cachedEntity = state.entities[action.id]
          if (cachedEntity) {
            actions.push(new <%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>Success(cachedEntity, { ...action.options }))

            if (action.options?.selectEntity) {
              actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(cachedEntity.id))
            }

            return from(actions)
          }
        }

        return this.<%= entity_plural%>DataService.get<%= capitalize(entity_singular)%>$(action.id, action.options).pipe(
          mergeMap(response => {
            const actions: Action[] = []

            actions.push(new <%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>Success(response, action.options))

            if (action.options?.selectEntity) {
              actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(response.id))
            }

            return from(actions)
          }),
          catchError(error => {
            this.handleErrorNotification(error, action.options)          
            return from([new <%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>Fail({ ...action.options, errors: error })])
          }),
        )
      }),
    ),
  )

  create<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>),
      mergeMap(action => {
        return this.<%= entity_plural%>DataService.create<%= capitalize(entity_singular)%>$(action.data, action.options).pipe(
          mergeMap(response => {
            const actions: Action[] = []
            
            this.handleSuccessNotification(action.options)
           
            actions.push(new <%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>Success(response, action.options))

            if (action.options?.selectEntity) {
              actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(response.id))
            }

            return from(actions)
          }),
          catchError(error => {
            this.handleErrorNotification(error, action.options)          
            return from([new <%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>Fail({ ...action.options, errors: error })])
          }),
        )
      }),
    ),
  )

  update<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>),
      mergeMap(action => {
        return this.<%= entity_plural%>DataService.update<%= capitalize(entity_singular)%>$(action.id, action.data, action.options).pipe(
          mergeMap(response => {
            const actions: Action[] = []

            this.handleSuccessNotification(action.options)         

            actions.push(new <%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>Success(response, action.options))

            if (action.options?.selectEntity) {
              actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(response.id))
            }

            return from(actions)
          }),
          catchError(error => {
            this.handleErrorNotification(error, action.options)          
            return from([new <%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>Fail({ ...action.options, errors: error })])
          }),
        )
      }),
    ),
  )

  delete<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>),
      mergeMap(action => {
        return this.<%= entity_plural%>DataService.delete<%= capitalize(entity_singular)%>$(action.id, action.options).pipe(
          mergeMap(deleted<%= capitalize(entity_singular)%> => {
            this.handleSuccessNotification(action.options)
            return [new <%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>Success(deleted<%= capitalize(entity_singular)%>, action.options)]
          }),
          catchError(error => {
            this.handleErrorNotification(error, action.options)          
            return from([new <%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>Fail({ ...action.options, errors: error })])
          }),
        )
      }),
    ),
  )

  handleSuccessNotification(options: ActionOptions){
    if (!options?.disableNotifications) {
      this.notification.success(this.translate.instant('shared.action_success'), '')
    }
  }

  handleErrorNotification(error: any, options: ActionOptions){
    if (options?.disableNotifications) {
      if (error.status === 404) {
        this.notification.error(
          this.translate.instant('errors.page_not_found'),
          this.translate.instant('errors.resource_not_found_description'),
        )
      }else if(error.status === 422){
        this.notification.error(
          this.translate.instant('errors.page_not_found'),
          this.translate.instant('errors.resource_not_found_description'),
        )
      } else {
        this.notification.error(this.translate.instant('errors.unknown_error'), this.translate.instant('errors.unknown_error_description'))
      } 
    }
  }
}

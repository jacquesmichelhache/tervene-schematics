import { Action } from '@ngrx/store'
import { IndexResponse } from '../index-response'
import {
  CreateActionOptions,
  DeleteActionOptions,
  ActionOptions,
  GetActionOptions,
  LoadActionOptions,
  UpdateActionOptions,
  ActionCompletedOptions,
  ClearEntities,
} from '../shared/advanced-store-classes'
import { <%= capitalize(entity_singular)%> } from './classes'

// CRUD operations
export const LOAD_<%= underscore(entity_plural).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Load <%= capitalize(entity_plural)%>'
export const LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS = '[<%= capitalize(entity_plural)%>] Load <%= capitalize(entity_plural)%> Success'
export const LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS_CACHE = '[<%= capitalize(entity_plural)%>] Load <%= capitalize(entity_plural)%> Success (Cache)'
export const LOAD_<%= underscore(entity_plural).toUpperCase() %>_FAIL = '[<%= capitalize(entity_plural)%>] Load <%= capitalize(entity_plural)%> Failed'

export const GET_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Get <%= capitalize(entity_singular)%>'
export const GET_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS = '[<%= capitalize(entity_plural)%>] Get <%= capitalize(entity_singular)%> Success'
export const GET_<%= underscore(entity_singular).toUpperCase() %>_FAIL = '[<%= capitalize(entity_plural)%>] Get <%= capitalize(entity_singular)%> Failed'

export const CREATE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Create <%= capitalize(entity_singular)%>'
export const CREATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS = '[<%= capitalize(entity_plural)%>] Create <%= capitalize(entity_singular)%> Success'
export const CREATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL = '[<%= capitalize(entity_plural)%>] Create <%= capitalize(entity_singular)%> Failed'

export const UPDATE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Update <%= capitalize(entity_singular)%>'
export const UPDATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS = '[<%= capitalize(entity_plural)%>] Update <%= capitalize(entity_singular)%> Success'
export const UPDATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL = '[<%= capitalize(entity_plural)%>] Update <%= capitalize(entity_singular)%> Failed'

export const DELETE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Delete <%= capitalize(entity_singular)%>'
export const DELETE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS = '[<%= capitalize(entity_plural)%>] Delete <%= capitalize(entity_singular)%> Success'
export const DELETE_<%= underscore(entity_singular).toUpperCase() %>_FAIL = '[<%= capitalize(entity_plural)%>] Delete <%= capitalize(entity_singular)%> Failed'

// other operations
export const UNSELECT_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Unselect <%= capitalize(entity_singular)%>'
export const SELECT_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Select <%= capitalize(entity_singular)%>'
export const CLEAR_<%= underscore(entity_plural).toUpperCase() %> = '[<%= capitalize(entity_plural)%>] Clear <%= capitalize(entity_plural)%>'
export const SET_ONE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>/cache] Set One <%= capitalize(entity_singular)%>'
export const SET_MANY_<%= underscore(entity_plural).toUpperCase() %> = '[<%= capitalize(entity_plural)%>/cache] Set Many <%= capitalize(entity_plural)%>'
export const REMOVE_ONE_<%= underscore(entity_singular).toUpperCase() %> = '[<%= capitalize(entity_plural)%>/cache] Remove One <%= capitalize(entity_singular)%>'
export const REMOVE_MANY_<%= underscore(entity_plural).toUpperCase() %> = '[<%= capitalize(entity_plural)%>/cache] Remove Many <%= capitalize(entity_plural)%>'
export const ACTION_DONE = '[<%= capitalize(entity_plural)%>] Action Done' // generic action_done. not used by default

// LOAD
export class Load<%= capitalize(entity_plural)%> implements Action {
  readonly type = LOAD_<%= underscore(entity_plural).toUpperCase() %>
  constructor(public params: any = {}, public options?: LoadActionOptions) {}
}
export class Load<%= capitalize(entity_plural)%>Success implements Action {
  readonly type = LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS
  constructor(public payload: IndexResponse<<%= capitalize(entity_singular)%>>, public options?: ActionCompletedOptions) {}
}
export class Load<%= capitalize(entity_plural)%>SuccessCache implements Action {
  readonly type = LOAD_<%= underscore(entity_plural).toUpperCase() %>_SUCCESS_CACHE
  constructor(public payload: IndexResponse<<%= capitalize(entity_singular)%>>, public options?: ActionCompletedOptions) {}
}
export class Load<%= capitalize(entity_plural)%>Fail implements Action {
  readonly type = LOAD_<%= underscore(entity_plural).toUpperCase() %>_FAIL
  constructor(public options?: ActionCompletedOptions) {}
}

// GET
export class Get<%= capitalize(entity_singular)%> implements Action {
  readonly type = GET_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public options?: GetActionOptions) {}
}
export class Get<%= capitalize(entity_singular)%>Success implements Action {
  readonly type = GET_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS
  constructor(public payload: <%= capitalize(entity_singular)%>, public options?: ActionCompletedOptions) {}
}
export class Get<%= capitalize(entity_singular)%>Fail implements Action {
  readonly type = GET_<%= underscore(entity_singular).toUpperCase() %>_FAIL
  constructor(public options?: ActionCompletedOptions) {}
}

// CREATE
export class Create<%= capitalize(entity_singular)%> implements Action {
  readonly type = CREATE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public data: Partial<<%= capitalize(entity_singular)%>>, public options?: CreateActionOptions) {}
}
export class Create<%= capitalize(entity_singular)%>Success implements Action {
  readonly type = CREATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS
  constructor(public payload: <%= capitalize(entity_singular)%>, public options?: ActionCompletedOptions) {}
}
export class Create<%= capitalize(entity_singular)%>Fail implements Action {
  readonly type = CREATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL
  constructor(public options?: ActionCompletedOptions) {}
}

// UPDATE
export class Update<%= capitalize(entity_singular)%> implements Action {
  readonly type = UPDATE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public data: Partial<<%= capitalize(entity_singular)%>>, public options?: UpdateActionOptions) {}
}
export class Update<%= capitalize(entity_singular)%>Success implements Action {
  readonly type = UPDATE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS
  constructor(public payload: <%= capitalize(entity_singular)%>, public options?: ActionCompletedOptions) {}
}
export class Update<%= capitalize(entity_singular)%>Fail implements Action {
  readonly type = UPDATE_<%= underscore(entity_singular).toUpperCase() %>_FAIL
  constructor(public options?: ActionCompletedOptions) {}
}

// DELETE
export class Delete<%= capitalize(entity_singular)%> implements Action {
  readonly type = DELETE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public options?: DeleteActionOptions) {}
}
export class Delete<%= capitalize(entity_singular)%>Success implements Action {
  readonly type = DELETE_<%= underscore(entity_singular).toUpperCase() %>_SUCCESS
  constructor(public payload: <%= capitalize(entity_singular)%>, public options?: ActionCompletedOptions) {}
}
export class Delete<%= capitalize(entity_singular)%>Fail implements Action {
  readonly type = DELETE_<%= underscore(entity_singular).toUpperCase() %>_FAIL
  constructor(public options?: ActionCompletedOptions) {}
}

// ACTIONS TO SET CACHE DIRECTLY
export class SetOne<%= capitalize(entity_singular)%> implements Action {
  readonly type = SET_ONE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public payload: Partial<<%= capitalize(entity_singular)%>>, public options?: ActionCompletedOptions) {}
}
export class SetMany<%= capitalize(entity_plural)%> implements Action {
  readonly type = SET_MANY_<%= underscore(entity_plural).toUpperCase() %>
  constructor(public payload: Partial<<%= capitalize(entity_singular)%>>[], public options?: ActionCompletedOptions) {}
}
export class RemoveOne<%= capitalize(entity_singular)%> implements Action {
  readonly type = REMOVE_ONE_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public id: number, public options?: ActionCompletedOptions) {}
}
export class RemoveMany<%= capitalize(entity_plural)%> implements Action {
  readonly type = REMOVE_MANY_<%= underscore(entity_plural).toUpperCase() %>
  constructor(public ids: number[] | ((entity: <%= capitalize(entity_singular)%>) => boolean), public options?: ActionCompletedOptions) {}
}

// MISC
export class Unselect<%= capitalize(entity_singular)%> implements Action {
  readonly type = UNSELECT_<%= underscore(entity_singular).toUpperCase() %>
  constructor() {}
}

export class Select<%= capitalize(entity_singular)%> implements Action {
  readonly type = SELECT_<%= underscore(entity_singular).toUpperCase() %>
  constructor(public entityId: number) {}
}

export class ActionDone implements Action {
  readonly type = ACTION_DONE
  constructor(public newState: any = {}, public options?: ActionOptions) {}
}

export class Clear<%= capitalize(entity_plural)%> implements Action {
  readonly type = CLEAR_<%= underscore(entity_plural).toUpperCase() %>
  constructor(public options?: ClearEntities) {}
}

export type Actions =
  | Load<%= capitalize(entity_plural)%>
  | SetOne<%= capitalize(entity_singular)%>
  | SetMany<%= capitalize(entity_plural)%>
  | RemoveOne<%= capitalize(entity_singular)%>
  | RemoveMany<%= capitalize(entity_plural)%>
  | Load<%= capitalize(entity_plural)%>Success
  | Load<%= capitalize(entity_plural)%>SuccessCache
  | Load<%= capitalize(entity_plural)%>Fail
  | Unselect<%= capitalize(entity_singular)%>
  | Select<%= capitalize(entity_singular)%>
  | Get<%= capitalize(entity_singular)%>
  | Get<%= capitalize(entity_singular)%>Success
  | Get<%= capitalize(entity_singular)%>Fail
  | Create<%= capitalize(entity_singular)%>
  | Create<%= capitalize(entity_singular)%>Success
  | Create<%= capitalize(entity_singular)%>Fail
  | Delete<%= capitalize(entity_singular)%>
  | Delete<%= capitalize(entity_singular)%>Success
  | Delete<%= capitalize(entity_singular)%>Fail
  | Clear<%= capitalize(entity_plural)%>
  | Update<%= capitalize(entity_singular)%>
  | Update<%= capitalize(entity_singular)%>Success
  | Update<%= capitalize(entity_singular)%>Fail
  | ActionDone

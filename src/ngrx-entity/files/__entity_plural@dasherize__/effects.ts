import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { of, from, forkJoin, Observable } from 'rxjs'
import { map, switchMap, catchError, withLatestFrom, concatMap, mergeMap, exhaustMap } from 'rxjs/operators'
import { NzNotificationService } from 'ng-zorro-antd/notification'

import * as EntitiesReducers from '../reducers'
import * as <%= capitalize(entity_plural)%>Actions from './actions'
import { TranslateService } from '@ngx-translate/core'
import { <%= capitalize(entity_plural)%>DataService } from './service'
import { ModalsService } from 'src/app/services/modals'
import { BaseEffectsClass } from 'src/app/store/base-effects-class'

@Injectable()
export class <%= capitalize(entity_plural)%>Effects extends BaseEffectsClass {
  BASE_ROUTE = '/:tenant/...' // schematic says user should complete base_route url path

  constructor(
    router: Router,
    rxStore: Store<any>,
    private actions$: Actions,
    private <%= entity_plural%>DataService: <%= capitalize(entity_plural)%>DataService,
    private translate: TranslateService,
    private notification: NzNotificationService,
    private modal: ModalsService,
  ) {
    super(router, rxStore)
  }

  load<%= capitalize(entity_plural)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>>(<%= capitalize(entity_plural)%>Actions.LOAD_<%= underscore(entity_plural).toUpperCase() %>),
      concatMap(action => of(action).pipe(withLatestFrom(this.rxStore.select(EntitiesReducers.get<%= capitalize(entity_plural)%>State)))),
      mergeMap(([action, latestState]) => {
        return this.<%= entity_plural%>DataService
          .get<%= capitalize(entity_plural)%>$({
            page: latestState.page,
            page_size: latestState.page_size,
            ...action.params,
          })
          .pipe(
            map(response => {
              return new <%= capitalize(entity_plural)%>Actions.ActionDone({ ...response }, { ...action.options, data: response.data })
            }),
            catchError(error => {
              if (!action.options.disableNotifications){
                this.handleErrors(error)
              }
              return from([new <%= capitalize(entity_plural)%>Actions.ActionDone({}, {  ...action.options, data: undefined, errors: error.error })])
            }),
          )
      }),
    ),
  )

  get<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.GET_<%= underscore(entity_singular).toUpperCase() %>),
      concatMap(action => of(action).pipe(withLatestFrom(this.rxStore.select(EntitiesReducers.get<%= capitalize(entity_plural)%>State)))),
      mergeMap(([action, latestState]) => {
        let found
        if (latestState.selected && latestState.selected.id === action.id) {
          found = latestState.selected
        }
        if (found) {
          const actions: Action[] = []
          if (!action?.options?.skipEntitySelection) {
            actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(found))
          }
          actions.push(new <%= capitalize(entity_plural)%>Actions.ActionDone({},  { ...action.options, data: found }))
          return from(actions)
        } else {
          return this.<%= entity_plural%>DataService.get<%= capitalize(entity_singular)%>$(action.id).pipe(
            mergeMap(response => {

              const actions: Action[] = []
              if (!action?.options?.skipEntitySelection) {
                actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(response))
              }
              actions.push(new <%= capitalize(entity_plural)%>Actions.ActionDone({},  { ...action.options, data: response }))

              return from(actions)
            }),
            catchError(error => {
              if (!action.options.disableNotifications){
                this.handleErrors(error)
              }
              return from([new <%= capitalize(entity_plural)%>Actions.ActionDone({}, { ...action.options, data: undefined, errors: error.error })])
            }),
          )
        }
      }),
    ),
  )

  create<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.CREATE_<%= underscore(entity_singular).toUpperCase() %>),
      mergeMap(action => {
        return this.<%= entity_plural%>DataService.create<%= capitalize(entity_singular)%>$(action.data).pipe(
          mergeMap(response => {
            if (!action.options.disableNotifications){
              this.notification.success(this.translate.instant('shared.action_success'), '')
            }           
           
            const actions: Action[] = []
            if (!action?.options?.skipEntitySelection) {
                actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(response))
            }
            actions.push(new <%= capitalize(entity_plural)%>Actions.ActionDone({ saveStatus: 'success' },  { ...action.options, data: response }))
            
            return from(actions)
          }),
          catchError(error => {
            if (!action.options.disableNotifications){
              this.handleErrors(error)
            }
            return from([new <%= capitalize(entity_plural)%>Actions.ActionDone({ saveStatus: 'failure' }, { ...action.options, data: undefined, errors: error.error })])
          }),
        )
      }),
    ),
  )

  update<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.UPDATE_<%= underscore(entity_singular).toUpperCase() %>),
      mergeMap(action => {
        return this.<%= entity_plural%>DataService.update<%= capitalize(entity_singular)%>$(action.id, action.data).pipe(
          mergeMap(response => {
            if (!action.options.disableNotifications){
              this.notification.success(this.translate.instant('shared.action_success'), '')
            }
           

            const actions: Action[] = []
            if (!action?.options?.skipEntitySelection) {
                actions.push(new <%= capitalize(entity_plural)%>Actions.Select<%= capitalize(entity_singular)%>(response))
            }
            actions.push(new <%= capitalize(entity_plural)%>Actions.ActionDone({ saveStatus: 'success' },  { ...action.options, data: response }))

            return from(actions)
          }),
          catchError(error => {
            if (!action.options.disableNotifications){
              this.handleErrors(error)
            }            
            return from([new <%= capitalize(entity_plural)%>Actions.ActionDone({ saveStatus: 'failure' }, { ...action.options, data: undefined, errors: error.error })])
          }),
        )
      }),
    ),
  )

  delete<%= capitalize(entity_singular)%>$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<<%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>>(<%= capitalize(entity_plural)%>Actions.DELETE_<%= underscore(entity_singular).toUpperCase() %>),
      mergeMap(action =>
        forkJoin([
          of(action),
          this.modal.confirm({
            nzTitle: this.translate.instant('shared.confirm_delete', {
              entity: this.translate.instant('entities.<%= underscore(entity_plural).toLowerCase() %>.this'),
            }),
            nzOkText: this.translate.instant('shared.yes'),
            nzOkType: 'danger',
            nzCancelText: this.translate.instant('shared.no'),
          }),
        ]),
      ),
      mergeMap(([action, confirm]) => {
        if (!confirm) {
          return of(new <%= capitalize(entity_plural)%>Actions.ActionDone({}, { ...action.options, data: undefined }))
        }

        return this.<%= entity_plural%>DataService.delete<%= capitalize(entity_singular)%>$(action.id).pipe(
          mergeMap(deleted<%= capitalize(entity_singular)%> => {
            this.notification.success(this.translate.instant('shared.action_success'), '')
            return [
              new <%= capitalize(entity_plural)%>Actions.ActionDone({ saveStatus: 'success' }, { ...action.options, data: deleted<%= capitalize(entity_singular)%> }),
              new <%= capitalize(entity_plural)%>Actions.Unselect<%= capitalize(entity_singular)%>(),
            ]
          }),
          catchError(error => {
            if (!action.options.disableNotifications){
              this.handleErrors(error)
            }            
            return from([new <%= capitalize(entity_plural)%>Actions.ActionDone({ saveStatus: 'failure' }, { ...action.options, data: undefined, errors: error.error })])
          }),
        )
      }),
    ),
  )

  handleErrors(error: any){
    if (error.status === 404) {
      this.notification.error(
        this.translate.instant('errors.page_not_found'),
        this.translate.instant('errors.resource_not_found_description'),
      )
    } else {
      this.notification.error(
        this.translate.instant('errors.unknown_error'),
        this.translate.instant('errors.unknown_error_description'),
      )
    }
  }
}

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IndexResponse } from '../index-response';
import { environment } from 'src/environments/environment';
import { setHttpParams } from 'src/app/helpers/http-helpers';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BaseEntityService } from '../base-entity-service'
import { <%= capitalize(entity_singular)%> } from './classes'
import * as <%= capitalize(entity_plural)%>Actions from './actions'
import { <%= capitalize(entity_plural)%>EntityOptions } from './actions'
import { EntityOperationsHelper } from '../entity-operations-helper';


// data service. This is where http requests are sent to API
@Injectable({ providedIn: 'root' })
export class <%= capitalize(entity_plural)%>DataService extends BaseEntityService {
  get<%= capitalize(entity_plural)%>$(params?: object): Observable<IndexResponse<<%= capitalize(entity_singular)%>>> {
    return this.http.get<IndexResponse<<%= capitalize(entity_singular)%>>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>`, { params: setHttpParams(params) })
  }

  get<%= capitalize(entity_singular)%>$(id: number): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.get<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>/${id}`)
  }

  create<%= capitalize(entity_singular)%>$(data: Partial<<%= capitalize(entity_singular)%>> | FormData): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.post<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>`, { <%= underscore(entity_singular).toLowerCase() %>: data })
  }

  update<%= capitalize(entity_singular)%>$(id: number, data: Partial<<%= capitalize(entity_singular)%>> | FormData): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.patch<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>/${id}`, { <%= underscore(entity_singular).toLowerCase() %>: data })
  }

  delete<%= capitalize(entity_singular)%>$(id: number): Observable<<%= capitalize(entity_singular)%>> {
    return this.http.delete<<%= capitalize(entity_singular)%>>(`${this.tenantApiUrl}/<%= underscore(entity_plural).toLowerCase() %>/${id}`)
  }
}

// Entity operations service facade implementing correlationId
@Injectable({ providedIn: 'root' })
export class <%= capitalize(entity_plural)%>EntityOperationsService {
  constructor(private rxStore: Store, private entityOperationsHelper: EntityOperationsHelper) {}

  getAll(options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = <%= capitalize(entity_plural)%>Actions.default<%= capitalize(entity_plural)%>Options): Observable<<%= capitalize(entity_singular)%>[]> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>[]>(corrId, <%= capitalize(entity_plural)%>Actions.ACTION_DONE)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>({page: 0, page_size: -1}, {  ...options, corrId: corrId }))
    return result$
  }

  getWithQuery(params: Object, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = <%= capitalize(entity_plural)%>Actions.default<%= capitalize(entity_plural)%>Options): Observable<<%= capitalize(entity_singular)%>[]> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>[]>(corrId, <%= capitalize(entity_plural)%>Actions.ACTION_DONE)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Load<%= capitalize(entity_plural)%>(params, {  ...options, corrId: corrId }))
    return result$
  }

  getById(id: number, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = <%= capitalize(entity_plural)%>Actions.default<%= capitalize(entity_plural)%>Options): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(corrId, <%= capitalize(entity_plural)%>Actions.ACTION_DONE)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Get<%= capitalize(entity_singular)%>(id, {  ...options, corrId: corrId }))
    return result$
  }

  add(<%= capitalize(entity_singular)%>: Partial<<%= capitalize(entity_singular)%>>, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = <%= capitalize(entity_plural)%>Actions.default<%= capitalize(entity_plural)%>Options): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(corrId, <%= capitalize(entity_plural)%>Actions.ACTION_DONE)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Create<%= capitalize(entity_singular)%>(<%= capitalize(entity_singular)%>, {  ...options, corrId: corrId }))
    return result$
  }

  update(id: number, <%= capitalize(entity_singular)%>: Partial<<%= capitalize(entity_singular)%>>, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = <%= capitalize(entity_plural)%>Actions.default<%= capitalize(entity_plural)%>Options): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(corrId, <%= capitalize(entity_plural)%>Actions.ACTION_DONE)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Update<%= capitalize(entity_singular)%>(id, <%= capitalize(entity_singular)%>, {  ...options, corrId: corrId }))
    return result$
  }

  delete(id: number, options: Partial<<%= capitalize(entity_plural)%>EntityOptions> = <%= capitalize(entity_plural)%>Actions.default<%= capitalize(entity_plural)%>Options): Observable<<%= capitalize(entity_singular)%>> {
    const corrId = this.entityOperationsHelper.generateUniqueId()
    const result$ = this.entityOperationsHelper.result<<%= capitalize(entity_singular)%>>(corrId, <%= capitalize(entity_plural)%>Actions.ACTION_DONE)
    this.rxStore.dispatch(new <%= capitalize(entity_plural)%>Actions.Delete<%= capitalize(entity_singular)%>(id, {  ...options, corrId: corrId }))
    return result$
  }
}

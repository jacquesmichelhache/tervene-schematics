import { AddEntityToReducerListContext } from "./add-entity-to-reducers-array";
import {Change, InsertChange} from "@schematics/angular/utility/change"
import { SchematicsException, Tree } from "@angular-devkit/schematics";
import { getSourceNodes } from "@schematics/angular/utility/ast-utils"
import * as ts from 'typescript';
import { strings } from '@angular-devkit/core';
// import * as fs from 'fs';

export function addEntityToReducersArrayChange(context: AddEntityToReducerListContext, tree: Tree): Change {
  let text = tree.read(context.path)
  if (!text) throw new SchematicsException('File does not exist.')
  let sourceText = text.toString('utf-8')

  // create the typescript source file
  let sourceFile = ts.createSourceFile(context.path, sourceText, ts.ScriptTarget.Latest, true)

  // get the nodes of the source file
  let nodes: ts.Node[] = getSourceNodes(sourceFile)

  // find the entitiesReducers node
  const entitiesReducersNode = nodes.find(n => n.kind === ts.SyntaxKind.Identifier && n.getText() === 'entitiesReducers')

  if (!entitiesReducersNode || !entitiesReducersNode.parent) {
    throw new SchematicsException(`expected entitiesReducers variable in ${context.path}`)
  }

  // define entitiesReducersNode's sibling and remove peopleNode from it
  let entitiesReducersNodeSiblings = entitiesReducersNode.parent.getChildren()
  let entitiesReducersNodeIndex = entitiesReducersNodeSiblings.indexOf(entitiesReducersNode)
  entitiesReducersNodeSiblings = entitiesReducersNodeSiblings.slice(entitiesReducersNodeIndex)

  // get entitiesReducers object literal expression from siblings, this means the sign "{"
  let entitiesReducersObjectLiteralExpressionNode = entitiesReducersNodeSiblings.find(n => n.kind === ts.SyntaxKind.ObjectLiteralExpression)

  if(!entitiesReducersObjectLiteralExpressionNode){
    throw new SchematicsException('entitiesReducers ObjectLiteralExpressionNode node is not defined')
  }

  let entitiesReducersListNode = entitiesReducersObjectLiteralExpressionNode.getChildren().find(n => n.kind === ts.SyntaxKind.SyntaxList)

  if(!entitiesReducersListNode){
    throw new SchematicsException('entitiesReducers list node is not defined')
  }

  let reducerToAdd = `
  ${context.options.entity_plural}: from${strings.capitalize(context.options.entity_plural)}.reducer,`

  return new InsertChange(context.path, entitiesReducersListNode.getEnd(), reducerToAdd)
}
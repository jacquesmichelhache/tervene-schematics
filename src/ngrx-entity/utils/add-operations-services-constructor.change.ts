import { AddEntityToReducerListContext } from "./add-entity-to-reducers-array";
import { Change, InsertChange } from "@schematics/angular/utility/change";
import { SchematicsException, Tree } from "@angular-devkit/schematics";
import { getSourceNodes } from "@schematics/angular/utility/ast-utils";
import * as ts from "typescript";
import { strings } from "@angular-devkit/core";
import { showTree } from "..";
// import * as fs from 'fs';

export function addOperationsServicesConstructorChange(
  context: AddEntityToReducerListContext,
  tree: Tree
): Change {
  let text = tree.read(context.path);
  if (!text) throw new SchematicsException("File does not exist.");
  let sourceText = text.toString("utf-8");

  // create the typescript source file
  let sourceFile = ts.createSourceFile(
    context.path,
    sourceText,
    ts.ScriptTarget.Latest,
    true
  );

  //showTree(sourceFile)

  // get the nodes of the source file
  let nodes: ts.Node[] = getSourceNodes(sourceFile);

  // find the entitiesReducers node
  const node = nodes.find(
    (n) =>
      n.kind === ts.SyntaxKind.Identifier && n.getText() === "AppEntityServices"
  );

  if (!node || !node.parent) {
    throw new SchematicsException(
      `expected to find a class declaration for 'AppEntityServices'`
    );
  }

  let constructorNode: ts.Node;
  node.parent.forEachChild((node) => {
    if (node.kind === ts.SyntaxKind.Constructor) {
      constructorNode = node;
    }
  });

  if (!constructorNode || !constructorNode.parent) {
    throw new SchematicsException(
      `expected to find a class constructor for 'AppEntityServices'`
    );
  }

  const constructorStartNode = constructorNode
    .getChildren()
    .find((n) => n.kind === ts.SyntaxKind.OpenParenToken);

  if (!constructorStartNode) {
    throw new SchematicsException(
      `expected to find a class constructor open parenthesis`
    );
  }

  let constructorImportString = `
    public ${context.options.entity_plural}: ${strings.capitalize(
    context.options.entity_plural
  )}EntityOperationsService,`;

  return new InsertChange(
    context.path,
    constructorStartNode.getEnd(),
    constructorImportString
  );
}

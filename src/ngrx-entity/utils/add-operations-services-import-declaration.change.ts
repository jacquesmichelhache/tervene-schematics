import { AddEntityToReducerListContext } from './add-entity-to-reducers-array'
import { Change, InsertChange } from '@schematics/angular/utility/change'
import { SchematicsException, Tree } from '@angular-devkit/schematics'
import { getSourceNodes } from '@schematics/angular/utility/ast-utils'
import * as ts from 'typescript'
import { strings } from '@angular-devkit/core'
// import * as fs from 'fs';

export function addOperationsServicesImportDeclarationChange(context: AddEntityToReducerListContext, tree: Tree): Change {
  let text = tree.read(context.path)
  if (!text) throw new SchematicsException('File does not exist.')
  let sourceText = text.toString('utf-8')

  // create the typescript source file
  let sourceFile = ts.createSourceFile(context.path, sourceText, ts.ScriptTarget.Latest, true)

  // get the nodes of the source file
  let nodes: ts.Node[] = getSourceNodes(sourceFile)

  // find the entitiesReducers node
  // console.log(nodes)
  const ImportDeclarationNodes = nodes.filter(n => n.kind === ts.SyntaxKind.ImportDeclaration)

  const entitiesReducersNode = ImportDeclarationNodes?.[ImportDeclarationNodes.length - 1]

  if (!entitiesReducersNode) {
    throw new SchematicsException(`expected at least one import declaration already present in ${context.path}`)
  }

  let reducerToAdd = `
import { ${strings.capitalize(context.options.entity_plural)}EntityOperationsService } from './${strings.dasherize(
    context.options.entity_plural,
  )}/service'`

  return new InsertChange(context.path, entitiesReducersNode.getEnd(), reducerToAdd)
}

import { AddEntityToReducerListContext } from "./add-entity-to-reducers-array";
import {Change, InsertChange} from "@schematics/angular/utility/change"
import { SchematicsException, Tree } from "@angular-devkit/schematics";
import { getSourceNodes } from "@schematics/angular/utility/ast-utils"
import * as ts from 'typescript';
import { strings } from '@angular-devkit/core';
import { showTree } from "..";
// import * as fs from 'fs';

export function addOperationsServicesConstructorBodyChange(context: AddEntityToReducerListContext, tree: Tree): Change {
  let text = tree.read(context.path)
  if (!text) throw new SchematicsException('File does not exist.')
  let sourceText = text.toString('utf-8')

  // create the typescript source file
  let sourceFile = ts.createSourceFile(context.path, sourceText, ts.ScriptTarget.Latest, true)

  // get the nodes of the source file
  let nodes: ts.Node[] = getSourceNodes(sourceFile) 

  // find the entitiesReducers node
  const node = nodes.find(n => n.kind === ts.SyntaxKind.Identifier && n.getText() === 'EntityOperationsService')

  if(!node || !node.parent){
    throw new SchematicsException(`expected to find a class declaration for 'EntityOperationsService'`)
  }

  let constructorNode: ts.Node;
  node.parent.forEachChild(node => {
    if(node.kind === ts.SyntaxKind.Constructor){
      constructorNode = node
    }
  })

  if (!constructorNode || !constructorNode.parent){
    throw new SchematicsException(`expected to find a class constructor for 'EntityOperationsService'`)
  }

  const constructoBlock = constructorNode.getChildren().find(n => n.kind === ts.SyntaxKind.Block)

  if (!constructoBlock){
    throw new SchematicsException(`expected to find a class constructor block for  'EntityOperationsService'`)
  } 

  const firstPunctuation = constructoBlock.getChildren().find(n => n.kind === ts.SyntaxKind.FirstPunctuation)

  if (!firstPunctuation){
    throw new SchematicsException(`expected to find a class constructor block open bracket punctuation for 'EntityOperationsService'`)
  } 


  let constructorBlockString = `
    this.${context.options.entity_plural} = ${context.options.entity_plural}EntityOperationsService;`

  return new InsertChange(context.path,  firstPunctuation.getEnd(), constructorBlockString)
}
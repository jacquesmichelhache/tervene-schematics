Angular Schematics allowing for automatic code generation of an Entity 
 

schematics can be installed and used by following steps bellow: 

step 1 (if not already installed): npm install -g @angular-devkit/schematics-cli 

step 2 (from inside the project repo): npm install tervene-schematics --save-dev 

step 3 (from inside the project repo): schematics tervene-schematics:ngrx-entity-advanced 

Step 4:  In the terminal, ngrx-entity schematic will ask you a few questions. Code will get generated after answering those questions 

IMPORTANT: Make sure you always use the latest version of tervene-schematics 

To update project with latest version: npm install tervene-schematics@latest --save-dev 


